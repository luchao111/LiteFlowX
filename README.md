# <center> LiteFlowX

<p align="center">

<a href="https://www.github.com/Coder-XiaoYi/LiteFlowX">
<img src="https://img.shields.io/badge/Github-blue?logo=github&logoColor=white&style=for-the-badge"/>
</a>
<a href='https://gitee.com/liupeiqiang/LiteFlowX/stargazers'>
<img src="https://img.shields.io/badge/Gitee-red?logo=gitee&logoColor=white&style=for-the-badge"/>
</a>

</p>

<p align="center">
<a href="https://plugins.jetbrains.com/plugin/19145-liteflowx">
<img src="https://img.shields.io/jetbrains/plugin/v/19145?logo=JetBrains&label=LiteFlowX&style=for-the-badge" />
</a>
<img src="https://img.shields.io/badge/IntelliJ--IDEA->=2018.3-brightgreen?logo=IntelliJ IDEA&style=for-the-badge"/>
<img src="https://img.shields.io/badge/license-Apache--2.0-blue?style=for-the-badge"/>

</p>


![LITEFLOWX](https://s1.ax1x.com/2022/05/12/O0Aw4K.png)

<div align="center">

English | [简体中文](./README.zh-CN.md)

</div>

<!-- Plugin description -->
<h3>LiteFlowX is designed to increase productivity when you use the <a href="https://github.com/dromara/liteflow">LiteFlow framework</a>.</h3>

## 🍬 LiteFlowX Features:
- Specific file Svg icons for easy identification of LiteFlow elements
- Able to identify Component, Node, Chain, Slot
- Jump between Component, Node, Chain and XML files
- Java code and Chain jump to each other
- Jump between Xml files and LiteFlow elements
- Auto-complete prompt for Xml, able to prompt Component, Node, Chain
- Provides LiteFLow toolbox
- Support file jump to liteflow.ruleSource property
- Both old and new versions of v2.6.x and v2.7.x are compatible
- ...more features to come

<!-- Plugin description end -->

## 🎉 Install LiteFlowX
For full details of install plugins, <a href="https://www.jetbrains.com/help/idea/managing-plugins.html">click me.</a>

> ### Install plugin from Marketplace
> 1. Press `Ctrl+Alt+S` to open the IDE settings and select **Plugins**.
> 2. Find the plugin named `LiteFlowX` in the **Marketplace** and click **install**.

> ### Install plugin form disk
> 1. Download the latest plugin archive on <a href="https://github.com/Coder-XiaoYi/LiteFlowX/releases">Releases</a>.
> 2. Press `Ctrl+Alt+S` to open the IDE settings and select **Plugins**.
> 3. On the Plugins page, click ⚙ and then click **Install Plugin from Disk...**.
> 4. Select the plugin archive file and click **OK**.
> 5. Click **OK** to apply the changes and restart the IDE if prompted.

## 🌈 Demonstration
### Java to Chain
![Java代码跳转到Chain](https://liteflow.yomahub.com/img/liteflowx/JavaToChain.gif)

### LiteFlowTool
![LiteFlowTool工具箱](https://liteflow.yomahub.com/img/liteflowx/LiteFlowTool.gif)

### Jump to ruleSource
![ruleSource跳转](https://liteflow.yomahub.com/img/liteflowx/ruleSourceJump.gif)

### Xml to Chain
![Xml跳转到Chain](https://liteflow.yomahub.com/img/liteflowx/XmlToChain.gif)

### Xml to Component
![Xml跳转Component](https://liteflow.yomahub.com/img/liteflowx/XmlToComponent.gif)
![Xml跳转Component2](https://liteflow.yomahub.com/img/liteflowx/XmlToManyComponent.gif)
